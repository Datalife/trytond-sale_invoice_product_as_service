# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, Bool

__all__ = ['Template', 'Product']


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    sale_as_service = fields.Boolean('Sale as service',
        states={'invisible': (Eval('type') == 'service')},
        depends=['type'])
    sale_service = fields.Many2One('product.product', 'Sale service',
        domain=[('type', '=', 'service')],
        states={
            'required': Bool(Eval('sale_as_service')),
            'invisible': ~Bool(Eval('sale_as_service'))
            },
        depends=['sale_as_service'])

    @staticmethod
    def default_sale_as_service():
        return False


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
