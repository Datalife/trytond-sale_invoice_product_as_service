# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_sale_invoice_product_as_service import suite

__all__ = ['suite']
